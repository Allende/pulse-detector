/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdio.h>
#include <stdint.h>          /* For uint32_t definition */
#include <stdbool.h>         /* For true/false definition */
#include <math.h>
#include "system.h"        /* System funct/params, like osc/peripheral config */
#include <libpic30.h>      /* Includes true/false definition                  */

#include "user.h"          /* User funct/params, such as InitApp              */
#include "mcc_generated_files/mcc.h" /* User funct/params, such as InitApp    */
#include "APDS9930.h"
#include "mcc_generated_files/test.h"
/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

#pragma udata

struct Int_Def_t val_init;
Data_base_Def data;
Data_base_Def data_min;
Data_base_Def data_max;
Data_base_Def data_mean;
Data_base_Def data_mean_hard;
Data_base_Def data_sesgo;
Data_base_Def data_sigma;
Data_dds      ddss;
Data_dds      *dds;  
Data_Period_Def *T_Glb;
bool          Scheduler_Glb;
uint32_t      Sample_Glb;

#pragma udata

#pragma idata
bool          Stt_Act= false;
#pragma idata

char string[6];
const uint8_t cosv[512]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,2,2,2,2,2,3,
        3,3,3,4,4,4,4,5,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9,10,10,11,11,11,12,12,13,13,13,14,
        14,15,15,16,16,16 ,17,17,18,18,19,19,20,20,21,21,22,22,23,23,24,24,25,25,26,27,
        27,28,28,29,29,30,
        30,31,32,32,33,33,34,35,35,36,36,37,37,38,39,39,40,40,41,42,42,43,44,44,
        45,45,46,47,47,48,48,49,50,50,51,51,52,53,53,54,55,55,56,56,57,58,58,59,
        59,60,61,61,62,62,63,64,64,65,65,66,67,67,68,68,69,69,70,70,71,72,72,73,
        73,74,74,75,75,76,76,77,77,78,78,79,79,80,80,81,81,82,82,83,83,84,84,85,
        85,85,86,86,87,87,88,88,88,89,89,89,90,90,91,91,91,92,92,92,93,93,93,93,
        94,94,94,95,95,95,95,96,96,96,96,96,97,97,97,97,97,98,98,98,98,98,98,98,
        99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,
        99,99,99,99,99,99,99,99,98,98,98,98,98,98,98,97,97,97,97,97,96,96,96,96,
        96,95,95,95,95,94,94,94,93,93,93,93,92,92,92,91,91,91,90,90,89,89,89,88,
        88,88,87,87,86,86,85,85,85,84,84,83,83,82,82,81,81,80,80,79,79,78,78,77,
        77,76,76,75,75,74,74,73,73,72,72,71,71,70,69,69,68,68,67,67,66,65,65,64,
        64,63,62,62,61,61,60,59,59,58,58,57,56,56,55,55,54,53,53,52,52,51,50,50,
        49,48,48,47,47,46,45,45,44,44,43,42,42,41,40,40,39,39,38,37,37,36,36,35,
        35,34,33,33,32,32,31,30,30,29,29,28,28,27,27,26,25,25,24,24,23,23,22,22,
        21,21,20,20,19,19,18,18,17,17,16,16,16,15,15,14,14,13,13,13,12,12,11,11,
        11,10,10,9,9,9,8,8,8,7,7,7,6,6,6,5,5,5,5,4,4,4,4,3,3,3,3,2,2,2,2,2,1,1,1,
        1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

const uint8_t sinv[512]= {50,50,51,51,52,53,53,54,54,55,56,56,57,57,58,59,59,60,60,61,62,62,63,
        63,64,65,65,66,66,67,68,68,69,69,70,70,71,71,72,73,73,74,74,75,75,76,
        76,77,77,78,78,79,79,80,80,81,81,82,82,83,83,84,84,84,85,85,86,86,87,87,
        87,88,88,89,89,89,90,90,90,91,91,91,92,92,92,93,93,93,94,94,94,94,95,95,
        95,95,96,96,96,96,97,97,97,97,97,98,98,98,98,98,98,98,99,99,99,99,99,99,
        99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,
        99,99,99,98,98,98,98,98,98,97,97,97,97,97,97,96,96,96,96,95,95,95,95,94,
        94,94,94,93,93,93,92,92,92,91,91,91,90,90,90,89,89,88,88,88,87,87,86,86,
        86,85,85,84,84,83,83,82,82,82,81,81,80,80,79,79,78,78,77,77,76,76,75,74,
        74,73,73,72,72,71,71,70,70,69,68,68,67,67,66,66,65,64,64,63,63,62,61,61,
        60,60,59,58,58,57,57,56,55,55,54,53,53,52,52,51,50,50,49,49,48,47,47,46,
        46,45,44,44,43,42,42,41,41,40,39,39,38,38,37,36,36,35,35,34,33,33,32,32,
        31,31,30,29,29,28,28,27,27,26,26,25,25,24,23,23,22,22,21,21,20,20,19,19,
        18,18,17,17,17,16,16,15,15,14,14,13,13,13,12,12,11,11,11,10,10,9,9,9,8,8,
        8,7,7,7,6,6,6,6,5,5,5,4,4,4,4,3,3,3,3,2,2,2,2,2,2,1,1,1,1,1,1,0,0,0,0,0,0
        ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,2,2,
        2,2,2,3,
        3,3,3,3,4,4,4,5,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9,10,10,10,11,11,12,12,12,13,
        13,14,14,15,15,15,16,16,17,17,18,18,19,19,20,20,21,21,22,22,23,23,24,24,
        25,25,26,26,27,28,28,29,29,30,30,31,31,32,33,33,34,34,35,36,36,37,37,38,
        39,39,40,40,41,42,42,43,43,44,45,45,46,46,47,48,48,49,49};


/* <Initialize variables in user.h and insert code for user algorithms.> */

/* TODO Initialize User Ports/Peripherals/Project here */
void InitApp(void){
    
    uint16_t i=0;
    Sample_Glb= 0;    
    Scheduler_Glb=0;
    data.reach=0;
    data.idx=0;
    data.roll=0;
    
    data_mean.reach=0;
    data_mean.idx=0;
    data_mean.roll=0;
    
    data_sesgo.reach=0;
    data_sesgo.idx=0;
    data_sesgo.roll=0;
    
    data_sigma.reach=0;
    data_sigma.idx=0;
    data_sigma.roll=0;
    
    data_max.reach=0;
    data_max.idx=0;
    data_max.roll=0;
    
    data_min.reach=0;
    data_min.idx=0;
    data_min.roll=0;    
    
    data_mean_hard.reach=0;
    data_mean_hard.idx=0;
    data_mean_hard.roll=0;
    
    i=0;
    do{
        data.D[i]=0;
        data_mean.D[i]=0;
        data_sesgo.D[i]=0;
        data_sigma.D[i]=0;
        data_min.D[i]=0;
        data_max.D[i]=0;
        data_mean_hard.D[i]=0;
        
    }while (i++<LNGTH);
    
}

void delay(void) {
    long i = 65535;
    while(i--);
}

uint16_t Idx_R(Data_base_Def *i){
    
    Data_base_Def *j;
    j=i;
    if(j->roll)        
        if(j->idx) // id distinto de cero          
            return (j->idx-1);
        else
            return (LNGTH-1);            
    else
        if(j->idx)           
            return (j->idx-1);
        else
            return (0); //returna cero            
        
}

uint16_t Idx_W(Data_base_Def *i){
    uint16_t a;      
    Data_base_Def *j;
    j=i;
    
    a= j->idx++;
    if(j->idx == LNGTH )
    {   
        j->roll= true;        
        j->idx= 0;
        return (a);      
    }   
    return (a);                  
}

uint16_t Idx_E(Data_base_Def *i){
    uint16_t a;      
    Data_base_Def *j;
    j=i;
    
    if(j->idx == 0 )
    {   
        j->idx=LNGTH-1;                
        return (LNGTH-1);      
    }       
    j->idx--;
    
    return (j->idx);                  
}

void Set_DDS(Data_dds *ds){
    
    Data_dds *d;
    static uint16_t i=0;
    static bool j=false;
    d= ds;
    /*
     * if(i== 600)
    {    
        j=!j;
        i=0;
    }
     * */
    d->mean=700+200*sin( 2*0.00031415* i);
    d->a= 1.5;      // Amplitud de depesion     
    d->clck= 0.1;    // Ruido sobrepico
    d->hz= 1;
    d->noise= data_sesgo.D[Idx_R(&data_sesgo)];
    i++;
             
}

uint16_t Fun_DDS(Data_dds *p){
    
    uint16_t t,y,n;   
    static uint16_t i= 0;
    Data_dds *q;
    const uint8_t  *u;
    
    q=p;    
    u= &cosv;
    t= i* q->hz;    
    y= q->mean;
   
       
        if(TMR1&0x0001)
            y-= (TMR1&0x000f);
        else
            y+= (TMR1&0x000f);
    
       
    if( t >= 471 )
    {
        n= (511*(t-471))/40;        
        y-= q->a * cosv[n];
        
    }
    if( t >= 491){
        n= (511*(t-491))/20;        
        y+= (q->clck)*(sinv[n]-50);
    }    
    if ( t > 511 )
        i=0;
    else          
        i++;
                
    return ((uint16_t)y);
}



uint8_t Inlet_Pulse(Data_Period_Def *t){    // t puntero para el vector interno
    
    static uint32_t T1,T2;
    static uint32_t time[3];
    static uint8_t i= 0;
    static enum state_base  st= init;    
    
    /*- Si es por ingreso , muestras . Es relativo
      - Si es por tick es absoluto                 */
    
    time[i++]= Sample_Glb;
    
    switch (st)
    {
        case init:            //i=1
            
            ++st;
        break;
        case take:            //i=2  
                        
            T1= time[1]- time[0];            
            ++st;
        break;
        case compare:         //i=3  
            
            T2= time[2]- time[1];
            if (T2 > (1.8*T1))
            {   
                /*SOLICITO NUEVO SCAN*/
                i= 0;
                st= init;
                t= time;
                return(half);
            }
            else if(T2 < (T1/2)) //LA VELOCIDAD AUMENTO MAS DEL DOBLE
            {
                /*ELIMINO POR ERROR, POSIBLE RUIDO. Este pulso no corresponde*/                
                i= 0;
                st= init;
                t= time;
                return(err);
            }                
            else 
            {                     
                T1= T2;             // Dejo periodo actual para el futuro
                time[1]= time[2];   // 
                i=2;                                
                t= time;                                
                return(ok);
            }                    
            
        break;
    }

}

bool Check_Frec(void){
    
}


void Take_Mesure(void)
{
    uint16_t *u;    
    uint16_t x;
    static uint16_t f=0;
    
    //static bool st = false;
    u= &x;
    Sample_Glb++;
    
    if (( f == 0 )||(f == 800)) // cada 10 segundos verifica el sesgo
    {
        /*Driver de Led bajo lumninacia para ver sesgo*/
        readProximity(u);       //descargo medicion anterior
        wireWriteDataByte(APDS9930_CONFIG, 1);
        setLEDDrive(LED_DRIVE_12_5MA);
        __delay_ms(100);
        readProximity(u);
        
        data_sesgo.D[Idx_W(&data_sesgo)]= *u;
                   
        /*Driver de Led subo lumninacia */
        wireWriteDataByte(APDS9930_CONFIG, 0);
        setLEDDrive(LED_DRIVE_100MA);
        __delay_ms(25);       
        readProximity(u);     //descargo medicion anterior  
        
        LATAbits.LATA2 = ~LATAbits.LATA2;                       
        f= 0;
    }
               
    //readProximity(u);
        
    dds= &ddss;
    
    Set_DDS(dds);
    /*Almaceno*/
    //data.D[Idx_W(&data)]= *u;
    data.D[Idx_W(&data)]= Fun_DDS(dds);
    //Add_item("r",*u);
    //printf(utoa(string,*u,10));
    //putchar('-');    

    //Toma 1
    //printf(utoa(string,*u,10));   
   // printf(utoa(string,val_init.sigma_std,10));
   // putchar(' ');
        
    f++;    
}       

void Set_Scheduler_Xplorer(void){
    Scheduler_Glb=true;    
}

bool Ckeck_Scheduler_Xplorer(void){    
    return Scheduler_Glb;    
}
void Clr_Scheduler_Xplorer(void){
    Scheduler_Glb= false;    
}

void Set_Xplorer(void){
    /*blanqueo los registros y regulo la velocidad de muestreo de forma escalonada*/
    uint16_t i;
            
    data_max.reach=0;
    data_max.idx=0;
    data_max.roll=0;
        
    data_min.reach=0;
    data_min.idx=0;
    data_min.roll=0;    
    
    data_mean_hard.reach=0;
    data_mean_hard.idx=0;
    data_mean_hard.roll=0;
    
    i=0;
    do{    
        data_min.D[i]=0;
        data_max.D[i]=0;
        data_mean_hard.D[i]=0;
        
    }while (i++<LNGTH);
    
    /** Depense el tipo de calculuss setear los correspondientes*/
    Stt_Act=false;  //calculuss returna a almacernar maximos

}

void Add_Pulse(void){
    
}



void Process(void){
    
    static uint32_t try= 0;
    static bool     Stt=  0;
    static bool     Stt1= 0;
    static bool     Stt2= 0;
    static uint8_t  St= take;
    static uint32_t T1,T2;
    
    switch ( Action_Out() )
    {
        case false:
            /* no exites pulso detectado, aun se aguarda medicion no asegura
               que haya sincronismo */
            if(try == TRY_LIMIT)
            {
                try= 0;
                /* define un scan*/
                Set_Scheduler_Xplorer();
                
            }else
            {
                if(Stt_Act) // espero a que haya pulso detectado inicial
                {
                    Stt1= true;
                }            
                else
                {
                    if (Stt1)
                    {                       
                        Stt2= true;
                    }            
                }    
            }
            if(Stt2)
                if (Stt_Act)            //Caso de activacion de pulso con remanecia fuera de tiempo
                {                       
                    switch(St)
                    {
                        case take:          //i=2                          
                            T1= T_Glb->time[1]- T_Glb->time[0];
                            ++St;
                        break;
                        case compare: 

                            T2= Sample_Glb - T_Glb->time[1];

                            if (T2 > (1.4*T1))
                            {   
                                /*SOLICITO NUEVO SCAN*/                                
                                Set_Scheduler_Xplorer();
                                Stt_Act= false;
                                St= take;
                            }                        
                        break;    
                    }   
                }
                else                    //Caso de no activacion de pulso , demasiado tiempo de espera
                {   
                    switch(St)
                    {
                        
                        case take:          //i=2                          
                            T1= T_Glb->time[1]- T_Glb->time[0];
                            ++St;
                        break;
                        case compare: 

                            T2= Sample_Glb - T_Glb->time[1];

                            if (T2 > (1.4*T1))
                            {   
                                /*SOLICITO NUEVO SCAN*/
                                
                                Set_Scheduler_Xplorer();
                                Stt= true;
                                St= take;
                            }                        
                        break;    
                    }                       
                }    
            
            try++;
            
        break;    
        case ok:
            /*realiz una suma exitosa de pulso de medicion */
            Add_Pulse();
            try= 0;
            
        break;    
        case half:        
            /*realiza la sintonizacion del registro*/
            Add_Pulse();
            Set_Scheduler_Xplorer();
            
        break;    
        case 0xff:        //error se filtra el pulso
            /*realiza la sintonizacion del registro*/
            Set_Scheduler_Xplorer();
            
        break;
    }   
    
    if(Stt_Act)
    {
        Stt= true;
    }            
    else
    {
        if (Stt)
        {
            if (Ckeck_Scheduler_Xplorer())
            {    
                Set_Xplorer();
                Clr_Scheduler_Xplorer;
            }    
            Stt= false;
        }            
    }    
    
}

uint8_t Action_Out(void){
    
    static bool st = false;    
    static bool i=0;
    static uint8_t xstd_low= 1; //STD_TRG_LW; //sigma para caso stado No Activo
    static uint8_t xstd_high= 1;//STD_TRG_HG; //sigma para caso stado Activo
    bool Stt_Act= false;
    uint8_t r= false;
    
    //Toma 6
    //printf(utoa(string, (0.3*val_init.sigma_std),10));
    //putchar(' ');
    //printf(utoa(string, val_init.Mean_std - STD_TRG_HG(val_init.sigma_std),10));
    //putchar(' ');
    //Toma 7
    //printf(utoa(string, val_init.Mean_std + STD_TRG_LW(val_init.sigma_std),10));
    //putchar(' ');
    //printf(utoa(string,i,10));
    Add_item("l-",val_init.Mean_std - STD_TRG_HG(val_init.sigma_std));
    Add_item("l+",val_init.Mean_std + STD_TRG_LW(val_init.sigma_std) ); //-0.2 *(val_init.sigma_std));
        
    if((val_init.Mean_std>UMBR_MIN_25MA) && (val_init.Mean_std!=0xFFFF))
    {                       
        if ( st )
        {   
            if (data_mean.D[Idx_R(&data_mean)] >= (val_init.Mean_std + STD_TRG_LW(val_init.sigma_std)))
            {    
                st= false;
                Stt_Act= false;
                //xstd_high = STD_TRG_HG;
                //xstd_low  = 100;//STD_TRG_LW;
                // putchar('>');
            }
        }            
        else
            if (data_mean.D[Idx_R(&data_mean)] <= (val_init.Mean_std - STD_TRG_HG(val_init.sigma_std)))
            {   
                
                st= true;
                Stt_Act= true;
                switch (Inlet_Pulse(T_Glb))
                {   
                    case err:
                            putchar('>');
                            r= 0xff;// error debo explorar                                             
                    break;    
                    case ok:
                            
                            r= true;
                            i^=true;                
                    break;
                    case half:
                            
                            putchar('>');
                            r= half;// error debo explorar                                             
                            i^=true;                
                    break;
                }                
                                
                //xstd_high = STD_TRG_LW;
                //xstd_low  = STD_TRG_HG-40;                          
            }
    }
    
    if(st && Stt_Act )
    {   
        Idx_E(&data_mean);   // Elimino dato de para que no se intruduzca en el Max
        LATA = 0xf0 | PORTA ;
    }else{  
        LATA = 0x0f & PORTA ;
    }            
          
    Add_item("S",(300+50*i));
    
    return(r);
    
}


//       origen          salida
void yhn(Data_base_Def *p,Data_base_Def *q){
    
    Data_base_Def *r,*s;
    static uint16_t mod= 0;
    struct H_Def{
       uint16_t H[LNGTH_HN];
       uint8_t idx;
       bool    roll; 
    }Hn;
    uint16_t i,n,j,k;   
    uint32_t sum= 0;
    
    i=0;
    do{
        Hn.H[i]=0;        
    }while (i++<LNGTH_HN);
    
    r= p; //base de se�al sin procesar
    s= q; //base de se�al procesada
    
    n= Idx_R(r);
    if(r->roll)
    {    
        if(n < LNGTH_HN) //n : indice
        {
            //n= Idx_R(p);            
            j= LNGTH_HN - (n+1);  //cantidad de elementos a leer en el roll pasado            
            k= LNGTH - j-1;       //indice aque apunta al elemento antes del roll
            
            for (i= 0 ; i <= n ; i++)       //leo despues del roll
            {
                Hn.H[i] = r->D[i];            
               
            }
            j=0;
            for (; i < LNGTH_HN; i++)       //leo antes del roll
            { 
                
                Hn.H[i] = r->D[k + j];
                j++;
               
            }
          /*
            for (i = 0; i < j; i++)
                Hn.H[i] = p->D[k + i];
            
            for (; i < LNGTH_HN; i++)
                Hn.H[i] = p->D[i - (j+1)];            
          */    
        }else
            if(n >= LNGTH_HN-1)
            {                
                //n= Idx_R(p);
                for (i = 0; i < LNGTH_HN ; i++)
                    Hn.H[i] = r->D[ (n - LNGTH_HN ) + i];          
            }
    }    
    else        
        if(n >= LNGTH_HN)
        {        
            for (i = 0; i < LNGTH_HN; i++)
                Hn.H[i] = r->D[n - LNGTH_HN + i];
        }    
        else
        {   
            //La salida directa sin procesar, no hay suficiente tramas
            //completa la demora de la salida del filtro
            i= 0;
            while( i <= n) 
                sum += r->D[i++];                                            
            sum/=i;
            s->D[Idx_W(s)]= sum;
            return;
        }  
    
    for(i=0; i<LNGTH_HN; i++)
        sum+= Hn.H[i];

    sum+= mod;
    mod=  sum % LNGTH_HN;    
    sum/= LNGTH_HN;

    s->D[Idx_W(s)]= sum;

    //if(499==q->idx)
    //printf(utoa(string,sum,10));
    //putchar(' ');
    //printf(utoa(string,q->idx,10));  
    //putchar('\n');
         
    return;
}

/*extraigo la media en bloques mas grandes de H(n) y variables*/
uint16_t mean (Data_base_Def *q, uint8_t frc){
    
    uint32_t acum= 0;
    uint16_t i= 0;
    uint16_t n,j,k;            
    Data_base_Def *p;
    
    p= q;
      
    j= 0;
    k= 0;
    n= Idx_R(p);   
    frc= LNGTH/frc;
    
    if(p->roll)
    {    
        if(n < frc) //n : indice
        {
            //n= Idx_R(p);            
            j= frc - (n+1);  //cantidad de elementos a leer en el roll pasado            
            k= LNGTH - j-1;       //indice aque apunta al elemento antes del roll
            
            for (i= 0 ; i <= n ; i++)       //leo despues del roll
            {
                acum += p->D[i];            
               
            }
            j=0;
            for (; i < frc; i++)       //leo antes del roll
            { 
                
                acum+= p->D[k + j];
                j++;
               
            }
              
        }else
            if(n >= frc)
            {             
                
                for (i = 0; i < frc ; i++)
                    acum+= p->D[ (n - frc ) + i];          
            }
    }    
    else
    {   
        acum= 0;
        if(n >= frc)
        {        
            for (i = 0; i < frc; i++)
                acum+= p->D[n - frc + i];
        }    
        else
        {   
            //La salida directa sin procesar, no hay suficiente tramas
            //completa la demora de la salida del filtro
            i= 0;
            while( i <= n ) 
                acum+= p->D[i++];                                            
            acum/=i;            
            
            return(acum & 0xFFFF);
        }  
    }
    acum/= frc;

    return ((uint16_t)acum);
  
}

void Calculus(void)
{
    uint16_t *p;
    uint16_t i,n,m,a,b;
    
     uint16_t min;
    static uint16_t min1;
    uint16_t max;
    static uint16_t f= 2;
    static uint16_t g= LNGTH/20;
    static uint8_t  st=3;
    static bool     st1= false;
    //for(i=0; (i< LNGTH) & (i< data.idx-1) ; i++)
    //        sum += *(p++);
    
    // Estimador de media con ultimas medidas 
    yhn(&data,&data_mean);
    
    Add_item("P",data_mean.D[Idx_R(&data_mean)]);
    // Estimador de media dura
    /*
     * if (try==0 || try==100)
    {
        n= Idx_W(&data_mean_hard);
        data_mean_hard.D[n]= mean(&data);
        try=0; 
    }
    try++;
    */
    //p= Hist(&data); //arroja los dos umbrales inferios y superior para el recorte
    
    //Recorto picos de se�al
    /*
    n= Idx_R(&data);
    
    if (data.D[n]< *p)
        data_mark.D[n] = *p;
    else
        if (data.D[n]> *(p+1))
            data_mark.D[n] = *(p+1);
        else
            data_mark.D[n] = data.D[n];
    */
    //Toma 5
    //printf(utoa(string,data_mark.D[n],10));
    //putchar('\n');
    //printf(utoa(string,*(p+1),10));
    //putchar('\n');

    //Extraigo superlativos cada LNGTH/20
     
    switch (st)
    {   
        case 1:
            if (f==0)
            {    
                LATAbits.LATA3++;

                m= Idx_R(&data_mean);

                min= mean(&data_min,FRCTN_MN);
                max= mean(&data_max,FRCTN_MN);
                
                yhn(&data_sesgo,&data_sigma); 

                a=0;
                b=0;

                if ( data_mean.D[m] < (max - data_sigma.D[Idx_R(&data_sigma)]) )
                        data_min.D[Idx_W(&data_min)] = data_mean.D[m];                

                if ( data_mean.D[m] > (min + data_sigma.D[Idx_R(&data_sigma)]) )
                    data_max.D[Idx_W(&data_max)] = data_mean.D[m];

                //if( data_mean.D[m] < (min + data_sigma.D[Idx_R(&data_sigma)]) )
                //{
                //    data_min.D[Idx_W(&data_min)] = data_mean.D[m];
                    //a= data_mean.D[m]- (min + data_sigma.D[Idx_R(&data_sigma)]);

                ///}else 
                ///    data_min.D[Idx_W(&data_min)] = data_mean.D[m];

                ///if ( data_mean.D[m] > (max - data_sigma.D[Idx_R(&data_sigma)]) )
                ///    data_max.D[Idx_W(&data_max)] = data_mean.D[m];

                  //  b=  (max - data_sigma.D[Idx_R(&data_sigma)])- data_mean.D[m];
                //else
                //    data_max.D[Idx_W(&data_max)] = data_mean.D[m];

                ///if (a && b )
                    ///if (a < b)
                    ///    data_min.D[Idx_W(&data_min)] = data_mean.D[m];
                    ///else
                    ///    <data_max.D[Idx_W(&data_max)] = data_mean.D[m];

                data_mean_hard.D[Idx_W(&data_mean_hard)] = FCTR_AVG((max-min))+ min;

                //val_init.Mean_std= max * 90  ;  
                //val_init.Mean_std/= 100  ;  
                //val_init.Mean_std>>=1;

                //val_init.sigma_std= data_sesgo.D[Idx_R(&data_sesgo)];

                f= 20;            
            }   
            f--;
            
            yhn(&data_sesgo,&data_sigma); 

            if ( (max-min) >= data_sigma.D[Idx_R(&data_sigma)] )
            {            
                data_max.D[Idx_W(&data_max)] = max;
                data_min.D[Idx_W(&data_min)] = min;

                if (g == 0)
                {                     
                    g= 2;//LNGTH/FRCTN_XPLR;

                    data_mean_hard.D[Idx_W(&data_mean_hard)]= FCTR_SRT((max-min))+ min;
                    //putchar('*');                
                    //putchar('*');
                    LATA = 0x80 | PORTA ;
                   /// if (f == 0)
                   // {     
                       // st= true;
                       // f= 20;
                        ////putchar('#');
                   // }
                   /// f--;
                }
                g--;                        
            }            
            
            
        break;
        
        case 2:
            
            //Extraigo superlativos cada LNGTH/20

            min= 5000;
            max= 100;
            
            n= Idx_R(&data_mean);

            if(data_mean.roll)
            {
                if( LNGTH/FRCTN_XPLR < n ) 
                {
                    for (i= n-LNGTH/FRCTN_XPLR ; i< n ; i++ )
                    {
                        if (data_mean.D[i]> max)
                            max = data_mean.D[i];

                        if ((data_mean.D[i]< min) && (min > UMBR_MIN_100MA))
                            min = data_mean.D[i];                
                    }                    
                }
                else
                {            
                    for (i= LNGTH -(LNGTH/FRCTN_XPLR) + n ; i<LNGTH ; i++ )
                    {
                        if (data_mean.D[i]> max)
                            max = data_mean.D[i];

                        if ((data_mean.D[i]< min ) && (min > UMBR_MIN_100MA))
                            min = data_mean.D[i];
                    }
                    for (i= 0 ; i< n ; i++ )
                    {
                        if (data_mean.D[i]> max)
                            max = data_mean.D[i];

                        if ((data_mean.D[i]< min) && (min > UMBR_MIN_100MA))
                            min = data_mean.D[i];
                    }
                }
                
            }else{

                if( LNGTH/FRCTN_XPLR > n ) 
                {
                    //Print_items();
                    //La salida directa sin procesar, no hay suficiente tramas
                    //completa la demora de la salida del filtro
                    i= 0;
                    while( i++ <= n)
                    {                  
                        if (data_mean.D[i]> max)
                            max = data_mean.D[i];

                        if ((data_mean.D[i]< min) && (min > UMBR_MIN_100MA))
                            min = data_mean.D[i];                         
                    }

                }                
                else 
                {

                    for (i= n-LNGTH/FRCTN_XPLR ; i< n ; i++ )
                    {
                        if (data_mean.D[i]> max)
                            max = data_mean.D[i];

                        if ((data_mean.D[i]< min) && (min > UMBR_MIN_100MA))
                            min = data_mean.D[i];                
                    }                    
                }                        
            }
            
            
            yhn(&data_sesgo,&data_sigma); 

            if ( (max-min) >= data_sigma.D[Idx_R(&data_sigma)] )
            {            
                data_max.D[Idx_W(&data_max)] = max;
                data_min.D[Idx_W(&data_min)] = min;

                if (g == 0)
                {                     
                    g= 2;//LNGTH/FRCTN_XPLR;

                    data_mean_hard.D[Idx_W(&data_mean_hard)]= FCTR_SRT((max-min))+ min;
                    //putchar('*');                
                    //putchar('*');
                    LATA = 0x80 | PORTA ;
                   /// if (f == 0)
                   // {     
                       // st= true;
                       // f= 20;
                        ////putchar('#');
                   // }
                   /// f--;
                }
                g--;                        
            }            
            
            
        break;
        
        case 3:
        //Extraigo superlativos cada LNGTH/20

            //if(!Stt_Act)
            //{           
                min= 1500;
                max= 100;
                //min1= 5000;
                n= Idx_R(&data_mean);
                
                if(data_mean.roll)
                {
                    if( LNGTH/FRCTN_XPLR < n ) 
                    {
                        for (i= n-LNGTH/FRCTN_XPLR ; i< n ; i++ )
                        {
                            if (data_mean.D[i]> max)
                                max = data_mean.D[i];

                            if ((data_mean.D[i]< min) && (min > UMBR_MIN_100MA))
                                min =data_mean.D[i];                
                        }                    
                    }
                    else
                    {            
                        for (i= LNGTH -(LNGTH/FRCTN_XPLR) + n ; i<LNGTH ; i++ )
                        {
                            if (data_mean.D[i]> max)
                                max = data_mean.D[i];

                            if ((data_mean.D[i]< min ) && (min > UMBR_MIN_100MA))
                                min = data_mean.D[i];
                        }
                        for (i= 0 ; i< n ; i++ )
                        {
                            if (data_mean.D[i]> max)
                                max = data_mean.D[i];

                            if ((data_mean.D[i]< min) && (min > UMBR_MIN_100MA))
                                min = data_mean.D[i];
                        }
                    }
                }else
                {

                    if( LNGTH/FRCTN_XPLR > n ) 
                    {
                        //Print_items();
                        //La salida directa sin procesar, no hay suficiente tramas
                        //completa la demora de la salida del filtro
                        i= 0;
                        while( i++ <= n)
                        {                  
                            if (data_mean.D[i]> max)
                                max = data_mean.D[i];

                            if ((data_mean.D[i]< min) && (min > UMBR_MIN_100MA))
                                min = data_mean.D[i];                         
                        }

                    }                
                    else 
                    {

                        for (i= n-LNGTH/FRCTN_XPLR ; i< n ; i++ )
                        {
                            if (data_mean.D[i]> max)
                                max = data_mean.D[i];

                            if ((data_mean.D[i]< min) && (min > UMBR_MIN_100MA))
                                min = data_mean.D[i];                
                        }                    
                    }                        
                }
            //}
            
            yhn(&data_sesgo,&data_sigma);
            
            if(!st1)
            {
                /*    disparo que difine un posible maximo        */
                if ( (max-min) > data_sigma.D[Idx_R(&data_sigma)] )
                {            
                    data_max.D[Idx_W(&data_max)]= max;
                    //data_min.D[Idx_W(&data_min)]= min;
                    if (g == 0)
                    {
                        g= 10;//LNGTH/FRCTN_XPLR;
                        data_max.D[Idx_W(&data_max)]= max;
                        val_init.Mean_std= mean(&data_max,FRCTN_MN);
                        /*
                        if (data_max.D[Idx_R(&data_max)]>data_max.D[Idx_R(&data_max)-1])
                            val_init.Mean_std+=0.1*(data_max.D[Idx_R(&data_max)]>data_max.D[Idx_R(&data_max)-1]);
                        else
                            val_init.Mean_std-=0.1*(data_max.D[Idx_R(&data_max)-1]>data_max.D[Idx_R(&data_max)]);
                        */
                        LATA = 0x80 | PORTA ;
                        __delay_ms(500);
                        if (f == 0)
                           st1= true;
                                                   
                        f--;
                        
                        min1= min;
                    }
                    g--;
                }
            }
            else
            {
                if ( ((max-min) < (0.5*data_sigma.D[Idx_R(&data_sigma)])) && !Stt_Act )
                {
                    data_max.D[Idx_W(&data_max)]= max;                                        
                    val_init.Mean_std= mean(&data_max,FRCTN_MN)- (FCTR_SGM*data_sigma.D[Idx_R(&data_sigma)]);                    
                    //min1= min;
                }
                else
                {
                    //@TODO En caso de desenganche tiene que volver al caso anterior de min max busqueda,
                    //se determina por medio de un seguimiento de frecuencia de pulso de deteccion
                    //Tambien un seguimiento por gradiente
                    //min1= min;
                    val_init.Mean_std= mean(&data_max,FRCTN_MN)-(FCTR_SGM*data_sigma.D[Idx_R(&data_sigma)]) ;
                }
            }
                                       
        break;
        
    }
    //val_init.Mean_std= mean(&data,FRCTN_MN);
    val_init.sigma_std= data_sigma.D[Idx_R(&data_sigma)];
    Add_item("Md",val_init.Mean_std);
    Add_item("Mx",max);
    
    LATAbits.LATA0++;
}

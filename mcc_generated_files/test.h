/* 
 * File:   test.h
 * Author: cba
 *
 * Created on November 11, 2020, 4:28 PM
 */

#ifndef TEST_H
#define	TEST_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* TEST_H */

struct Data_Test_Def_t{
    char Tipo[5];
    uint16_t Data;
};

struct Data_base_Test_Def_t{
    struct   Data_Test_Def_t msg[10];
    uint16_t index;
};

typedef struct Data_base_Test_Def_t Data_base_Test_Def;


void clear_items(void);
void Add_item( char *, uint16_t );
void Print_items(void);
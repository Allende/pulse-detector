

#include <stdio.h>
#include <string.h>
#include <stdint.h>          /* For uint32_t definition */
#include <stdbool.h>
#include <stdlib.h>         /* For true/false definition */

#include "test.h"

Data_base_Test_Def M;

void clear_items(void){
    M.index = 0;
    
}
void Add_item( char *p, uint16_t c){
   
    strcpy(M.msg[M.index].Tipo,p);
    M.msg[M.index].Data= c;
    M.index++;
    
}
void Print_items(void){
    
    char string[6];
    uint8_t i;
    
    for(i=0; i< M.index ;i++ )
    {
        printf(M.msg[i].Tipo);
        putchar(':');
        putchar(' ');
        printf(utoa(string,M.msg[i].Data,10));
        putchar(' ');
        
    }
    putchar('\n');
    clear_items();
}
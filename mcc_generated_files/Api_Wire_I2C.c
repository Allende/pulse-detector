
#include "../system.h"          /* variables/params used by system.c */
#include "libpic30.h"
#include "i2c1.h"
#include "Api_Wire_I2C.h"
#include <stdio.h>       /* Includes true/false definition                  */

bool Wire_Put(uint8_t *pData,uint16_t nCount,uint8_t Slave ){
    
    //uint8_t              writeBuffer[1];
    uint16_t             retryTimeOut, slaveTimeOut;
    I2C1_MESSAGE_STATUS status = I2C1_MESSAGE_PENDING;
            
    // build the write buffer first
    // data to be written
    //writeBuffer[0] = *pData;

    // Now it is possible that the slave device will be slow.
    // As a work around on these slaves, the application can
    // retry sending the transaction
    retryTimeOut = 0;
    slaveTimeOut = 0;

    while(status != I2C1_MESSAGE_FAIL)
    {
        // write one byte to EEPROM (3 is the number of bytes to write)
        I2C1_MasterWrite( pData,
                          nCount,
                          Slave,
                          &status );

        // wait for the message to be sent or status has changed.
        while(status == I2C1_MESSAGE_PENDING)
        {
            // add some delay here
             __delay_us(10);
            // timeout checking
            // check for max retry and skip this byte
            if (slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT){
                //putchar('a');
                break;
            }
            else
                slaveTimeOut++;
        }
                         
        if (status == I2C1_MESSAGE_COMPLETE)
        {
            //putchar('b');
            return true;
        }
        //if ((slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT) )
        //    break;

        // if status is  I2C1_MESSAGE_ADDRESS_NO_ACK,
        //               or I2C1_DATA_NO_ACK,
        // The device may be busy and needs more time for the last
        // write so we can retry writing the data, this is why we
        // use a while loop here

        // check for max retry and skip this byte
        if (retryTimeOut == SLAVE_I2C_GENERIC_RETRY_MAX){
            //putchar('c');
            return false;
        }    
        else
            retryTimeOut++;
    }

    if (status == I2C1_MESSAGE_FAIL)
    {   
        //putchar('d');
        return false;
    }
    
    return true;
}

 bool Wire_Get(uint8_t *pData,uint16_t nCount,uint8_t Slave ){
 
    I2C1_MESSAGE_STATUS status;
    
    uint16_t retryTimeOut, slaveTimeOut;
         
    // Now it is possible that the slave device will be slow.
    // As a work around on these slaves, the application can
    // retry sending the transaction
    retryTimeOut = 0;
    slaveTimeOut = 0;
  
    while(status != I2C1_MESSAGE_FAIL)
    {
        // write one byte to EEPROM (2 is the count of bytes to write)
        I2C1_MasterRead(    pData,
                            nCount,
                            Slave,
                            &status);

        // wait for the message to be sent or status has changed.
        while(status == I2C1_MESSAGE_PENDING)
        {
            // add some delay here
             __delay_us(10);    
            // timeout checking
            // check for max retry and skip this byte
            if (slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT)
                break;
            else
                slaveTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
            return(true);

        // if status is  I2C1_MESSAGE_ADDRESS_NO_ACK,
        //               or I2C1_DATA_NO_ACK,
        // The device may be busy and needs more time for the last
        // write so we can retry writing the data, this is why we
        // use a while loop here

        // check for max retry and skip this byte
        if (retryTimeOut == SLAVE_I2C_GENERIC_RETRY_MAX)
            return false;
        else
            retryTimeOut++;
    }

    // exit if the last transaction failed
    if (status == I2C1_MESSAGE_FAIL)
    {
        return(false);
        
    }
    return(true);
          
}

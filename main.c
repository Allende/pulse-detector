/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#endif

#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>
#include <stdio.h>

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include <libpic30.h>      /* Includes true/false definition                  */
#include "user.h"          /* User funct/params, such as InitApp              */
#include "mcc_generated_files/mcc.h" /* User funct/params, such as InitApp    */
#include "APDS9930.h"
#include "mcc_generated_files/test.h"


/******************************************************************************/
/* Global Variable Declaration                                                */
/******************************************************************************/

/* i.e. uint16_t <variable_name>; */
int __C30_UART=2;

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/


int16_t main(void)
{
    uint16_t v[1];
    
        
    /* Configure the oscillator for the device */
    SYSTEM_Initialize();
    ConfigureOscillator();
    init_APDS9930();   
    /* Initialize IO ports and peripherals */
    InitApp();
    
    enablePower();
    setMode(PROXIMITY,ON); 
    setProximityGain(PGAIN_1X);
    setLEDDrive(LED_DRIVE_12_5MA);
    setProximityDiode(2);
    /* TODO <INSERT USER APPLICATION CODE HERE> */
    TRISA = 0;
    wireWriteDataByte(APDS9930_CONFIG, 0);
    setLEDDrive(LED_DRIVE_100MA);
    __delay_ms(25);        
    readProximity(v); 
    __delay_ms(100);
    while(1)
    {
        /*Driver del motor*/
        
        //Move_Wheel();
        Pulse_Toggle_Meter_Roll();
        __delay_ms(14);        
        
        Take_Mesure();    //deberia ser llamadada por un semaforo que asegure el tiempo

        Calculus();       //deberia ser llamadada 
        
        Process();
      
        Print_items();        
        
    }
}

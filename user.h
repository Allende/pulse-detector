/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/
#define LNGTH            100
#define LNGTH_HN         20
#define FRCTN_MN         5
#define FRCTN_XPLR       2            //denominador para determinar facto de exploracion de superlativos 
#define FCTR_SRT(a)    ((5*a)/10)      //Tara para valor medio 18�/360� mayor
#define FCTR_AVG(a)    ((5*a)/10)      //Tara para valor medio 18�/360� mayor
#define FCTR_SGM        (0.4) 
#define STD_TRG_LW(a)    ((2*a)/10)    //pondera menor
#define STD_TRG_HG(a)    ((2*a)/10)    //pondera mayor

#define SLT_H            2
#define STD_H            8    // porcentaje histrograma
#define LMT_H            (LNGTH*(STD_H/10))
#define UMBR_MIN_100MA   400
#define UMBR_MIN_50MA    300
#define UMBR_MIN_25MA    100
#define LGTH_DDS         512;
#define TRY_LIMIT        10000   


#define IDX_R(a)         (a->idx-1)
#define IDX_W(a)         a->idx 

/* TODO Application specific user parameters used in user.c may go here */
/*Data estructures*/
struct Data_base_Def_t{    
    bool     roll;
    uint16_t reach; //sirve para os promedios
    uint16_t D[LNGTH];       
    uint16_t idx;
};

struct Int_Def_t{
    
    uint16_t Mean_std;
    uint16_t sigma_std;
    
};

struct Data_base_dds_t{
    
    uint16_t mean;
    uint16_t hz;
    uint16_t noise; 
    uint16_t clck; 
    uint16_t a ;
    
};

struct Data_base_Period_Def_t{

    uint16_t time[3];
    bool     roll;
    uint8_t  idx;
    
};

typedef struct  Data_base_Period_Def_t Data_Period_Def; 
typedef struct  Data_base_dds_t        Data_dds; 
typedef struct  Data_base_Def_t        Data_base_Def;


typedef enum state_base { init=0, take , compare};
typedef enum tipo_base { err=0, ok , half};


/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

/* TODO User level functions prototypes (i.e. InitApp) go here */

void InitApp(void);         /* I/O and Peripheral Initialization */
void delay(void);

uint16_t Idx_R(Data_base_Def *);
uint16_t Idx_W(Data_base_Def *);
uint16_t Idx_E(Data_base_Def *);

void Set_DDS(Data_dds* );
uint16_t Fun_DDS(Data_dds*);

uint8_t Inlet_Pulse(Data_Period_Def *);
bool Check_Frec(void);
void Take_Mesure(void);

void Add_Pulse(void);


void Set_Scheduler_Xplorer(void);
bool Ckeck_Scheduler_Xplorer(void);
void Clr_Scheduler_Xplorer(void);
void Set_Xplorer(void);

void Process(void);
uint8_t Action_Out(void);
void yhn(Data_base_Def *,Data_base_Def *);    
uint16_t mean (Data_base_Def*, uint8_t );
//uint16_t * Hist( Data_base_Def* );
void Calculus(void);